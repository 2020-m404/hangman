﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Hangman
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // This field is readonly so it can't be changed (accidentally).
        private readonly string _word;

        private int _lives = 5;

        // This field initially contains "_" as placeholder characters for every letter of the guess _word.
        // Correct guesses lead to uncoverings of corresponding characters in this array.
        // At the end of the (won) game, the character sequence of this field is identical to the _word string.
        private readonly char[] _guessWordCharacters;

        public MainWindow()
        {
            InitializeComponent();

            // Retrieve word from WordProvider and assign it to the readonly field.
            _word = WordProvider.GetWord();

            // Generate the placeholder characters for every letter in the guess _word.
            _guessWordCharacters = Enumerable.Repeat('_', _word.Length).ToArray();

            // Call method which is responsible for printing the (covered) guess word in the GUI.
            PrintGuessWord();

            // Print guess _word to debug console
            Debug.WriteLine($"Guess word: {_word}");
        }

        /// <summary>
        /// This method prints the covered guessword in the GUI.
        /// For better readability, the individual characters/placeholder are separated by a "space" (" ") character.
        /// </summary>
        private void PrintGuessWord()
        {
            // WordBox is the name of the corresponding WPF TextBox element which contains the current (covered) guess word.
            // The Join method "glues" the elements of the given array together,
            // using the provided "separator" string in between every character.
            WordBox.Text = string.Join(" ", _guessWordCharacters);

            // Winning condition: If the guess word with uncovered characters equals the original guess _word,
            // the player has discovered the complete guess word successfully.
            if (string.Join("", _guessWordCharacters) == _word)
            {
                // Print a winner message...
                MessageBox.Show($"You correctly guessed the word \"{_word}\"", "YOU WIN");

                // ...and shut down the running application.
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Click handler for all buttons A - Z of the GUI.
        /// A click on any of the buttons calls this method.
        /// </summary>
        /// <param name="sender">The button which was clicked</param>
        /// <param name="e">Click event arguments</param>
        private void LetterButtonClick(object sender, RoutedEventArgs e)
        {
            // Since we know, only clicks on button objects can lead to executions of this method,
            // we can safely cast the unspecific "object" sender to a Button object.
            Button clickedButton = (Button)sender;

            // Here we use the fact, that every buttons content (the "displayed text") matches the guessed letter.
            string clickedLetter = clickedButton.Content.ToString();

            // The actual guessing logic is not implemented in the click handler.
            // So, we call the PlayLetter method with the guessed character as argument.
            PlayLetter(clickedLetter[0]);
        }

        /// <summary>
        /// There are two different options which a player can use to guess a character:
        ///     - Using the provided "keyboard" (buttons) in the GUI
        ///     - Using the actual hardware keyboard by pressing any character
        ///     
        /// Independent of the guessing method, both input variants finally lead to a call of this method.
        /// </summary>
        /// <param name="letter">The character which the player guessed.</param>
        private void PlayLetter(char letter)
        {
            // Check whether the guessed character is part of the guess _word.
            if (_word.Contains(letter))
            {
                // Uncover the guessed letter and update the guess word in the GUI.
                UnmaskLetter(letter);
                PrintGuessWord();
            }
            else
            {
                // Guess was wrong --> Decrease the players live count and update the images source.
                _lives--;
                HangmanImage.Source = new BitmapImage(new Uri($"lives_{_lives}.png", UriKind.Relative));
            }

            // Check whether the player eventually lost the game (when having 0 lives remaining).
            if (_lives == 0)
            {
                // Show the word which should have been guessed...
                MessageBox.Show($"Correct word was: {_word}", "GAME OVER");

                // ... and shut down the application.
                Application.Current.Shutdown();
            }

            // Disable the button which corresponds to the guessed letter.
            DisableButton($"{letter}");
        }

        /// <summary>
        /// This method disables a button in the GUI.
        /// Disabling happens after a letter was played - either by clicking on the corresponding button
        /// or by pressing the letter on the physical keyboard.
        /// </summary>
        /// <param name="letter">The letter for which the button should be disabled</param>
        private void DisableButton(string letter)
        {
            // Retrieve the WPF object within the MainGrid object by name.
            object button = MainGrid.FindName($"Button{letter}");

            // If the retrieved object is of type Button, it will be disabled.
            if (button is Button)
            {
                ((Button)button).IsEnabled = false;
            }
        }

        /// <summary>
        /// Uncover a letter in the guess word, which initially contains only placeholder for each character
        /// in the guess _word. It is not required for the letter to be contained in the guess _word for the
        /// method to work properly: Nothing happens if the given letter is not part of the guess _word.
        /// If the guess _word contains the given letter multiple times, every occurence is uncovered.
        /// </summary>
        /// <param name="letter">The letter to uncover within the guess _word.</param>
        private void UnmaskLetter(char letter)
        {
            // Initialize index so that searching initially starts at the first character.
            int index = -1;

            do
            {
                // The IndexOf method returns the (0 based) position, at which the given character (first argument)
                // occures first. Only positions starting at the second arguments position are considered.
                // Note, that the index is set to the position, at which the relevant letter is found. On the next run
                // of the do-while loop, searching starts exactly after that position.
                index = _word.IndexOf(letter, index + 1);

                // If the given letter was NOT found withing the guess _word, the return value of IndexOf method is -1.
                if (index >= 0)
                {
                    // The placeholder in the _guessWordCharacters array is repaced with the actual character.
                    _guessWordCharacters[index] = letter;
                }
            } while (index >= 0);
        }

        /// <summary>
        /// Event handler for key presses on the physical keyboard.
        /// The event listener is registered in the MainWindow.xaml file as KeyDown property on the Window object.
        /// The key which was pressed is contained in the KeyEventArgs argument.
        /// </summary>
        /// <param name="sender">Window object, which triggered the key down event.</param>
        /// <param name="e">Argument containing further information about the occured event.</param>
        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            // Retrieve letter from argument
            char letter = e.Key.ToString()[0];

            // If letter is valid/relevant character...
            if (letter >= 'A' && letter <= 'Z')
            {
                // ... us it as character to guess.
                PlayLetter(letter);
            }
        }
    }
}